/**
 * Webpack main entry file
 */

import Vue from 'vue'
import App from './App'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VCalendar from 'v-calendar'
import api from './helpers/api'
import router from './helpers/router'
import config from './helpers/config'
import lang from './helpers/lang'
import colors from './helpers/colors'
import events from './helpers/events'
import methods from './mixins/methods'
import AppControls from './components/app-controls'
import AppAvatar from './components/app-avatar'
import LoadingOverlay from './components/loading-overlay'
import Page from './components/page'
import ErrorDialog from './components/error-dialog'
import ExternalLinks from './components/external-links'
import '@mdi/font/css/materialdesignicons.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-icons/iconfont/material-icons.css'

Vue.use(VueAxios, axios)
Vue.use(Vuetify)
Vue.use(VCalendar)

Vue.component('app-controls', AppControls)
Vue.component('app-avatar', AppAvatar)
Vue.component('loading-overlay', LoadingOverlay)
Vue.component('page', Page)
Vue.component('error-dialog', ErrorDialog)
Vue.component('external-links', ExternalLinks)

Vue.prototype.$lang = lang
Vue.prototype.$config = config
Vue.prototype.$colors = colors
Vue.prototype.$api = api
Vue.prototype.$events = events

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  template: '<App/>',
  components: {
    App
  },
  mixins: [
    methods
  ],
  data () {
    return {
      colors,
      config
    }
  },
  created () {
    this.onCreated()
  },
  methods: {
    /**
     * Handler for the $created event of the component
     * @method onCreated
     * @return {void}
     */
    onCreated () {
      this.addAxiosInterceptors()
      this.setDocumentTitle()
    },
    /**
     * Set Axios interceptors
     * @method addAxiosInterceptors
     * @return {void}
     */
    addAxiosInterceptors () {
      const events = this.$events
      const ajax = events.ajax
      const interceptors = this.$http.interceptors
      interceptors.request.use(config => {
        this.$emit(ajax, true)
        return config
      }, function (error) {
        this.$emit(ajax, false)
        return Promise.reject(error)
      })

      interceptors.response.use(response => {
        window.scrollTo(0, 0)
        this.$emit(ajax, false)
        return response
      }, (error) => {
        this.$emit(ajax, false)
        this.$emit(events.error, false)
        return Promise.reject(error)
      })
    }
  }
})
