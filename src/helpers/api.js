/**
 * API route definitions
 * @module api
 */

import config from './config'

const api = {
  bpi: {
    /**
     * API resource for getting BPI data from CoinDesk
     * @method url
     * @param {string} start [start=config.minDate]
     * @param {string} end [end=config.today]
     * @return {string}
     */
    url: (start = config.minDate, end = config.today) => {
      return `https://api.coindesk.com/v1/bpi/historical/close.json?start=${start}&end=${end}`
    },
    meta: '/bpi',
    methods: ['GET'],
    description: 'Get historical BPI data from CoinDesk by start and end date range'
  },
  index: {
    url: null,
    meta: '/index',
    methods: ['GET'],
    description: 'Get the contents of the home page'
  },
  about: {
    url: null,
    meta: '/about',
    methods: ['GET'],
    description: 'Get the contents of the about page'
  }
}

export default api
