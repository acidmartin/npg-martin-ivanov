/**
 * Application color schemes
 * @module colorSchemes
 */

const colorSchemes = {
  red: {
    primary: 'red'
  },
  pink: {
    primary: 'pink'
  },
  indigo: {
    primary: 'indigo'
  },
  green: {
    primary: 'green'
  },
  orange: {
    primary: 'orange'
  }
}

export default colorSchemes
