/**
 * Application localization strings
 * @module config
 */

const lang = {
  about: 'About',
  home: 'Home',
  errorDialogTitle: 'API Error',
  copyChartUrl: 'Permalink',
  ok: 'Ok',
  close: 'Close',
  externalLinksTitle: 'My other Vue projects',
  externalLinksSubTitle: 'Links to other personal Vue projects I\'ve built',
  permalinkCopied: 'Copied chart permalink',
  lastUpdated: 'Last updated on',
  allTime: 'All Time...',
  viewCharts: 'View Charts',
  exportChart: 'Export',
  bpiDataByYear: 'BPI Data by Year',
  allowMultiple: 'Multiple year selection',
  byYear: 'Year by year',
  customDateRange: 'Custom Range',
  applyCustomRange: 'Apply Custom Range',
  httpErrorMessage: `<p>Bummer... An HTTP error has occurred.</p>
    <p>Most probably it was caused by incorrect setting of date range (what about swapped dates)?</p>
    <p>Click OK to close this message, try again with different settings.</p>`,
  /**
   * @function setColorScheme
   * @param {string} color
   * @return {string}
   */
  setColorScheme: (color) => {
    return `Set ${color} color scheme`
  },
  /**
   * @function setTheme
   * @param {boolean} dark
   * @return {string}
   */
  setTheme: (dark) => {
    return `Set ${dark ? 'light' : 'dark'} theme`
  },
  /**
   * @function chartLabel
   * @param {string} date
   * @return {string}
   */
  chartLabel (date) {
    return `From ${date} onwards`
  }
}

export default lang
