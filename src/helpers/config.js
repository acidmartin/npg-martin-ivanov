/**
 * Application configuration
 * @module config
 */

import moment from 'moment'
import { description, version, author, appTitle } from '../../package'

const bpiApiDateFormat = 'YYYY-MM-DD'
const defaultColorScheme = 'indigo'
const dark = localStorage.getItem('dark')

const config = {
  author,
  version,
  appTitle,
  description,
  bpiApiDateFormat,
  defaultColorScheme,
  appIcon: 'mdi-chart-bar',
  colorScheme: localStorage.getItem('scheme') || defaultColorScheme,
  dark: dark ? JSON.parse(dark) : true,
  today: moment(new Date()).format(bpiApiDateFormat),
  exportFilename: 'coindesk-bpi-data',
  chartExportOptions: [{
    icon: 'mdi-image',
    type: 'png',
    label: 'Image',
    enabled: true
  }, {
    icon: 'mdi-json',
    type: 'json',
    label: 'JSON',
    enabled: true
  }, {
    icon: 'mdi-file-delimited',
    type: 'csv',
    label: 'CSV',
    enabled: true
  }],
  minDate: '2010-07-17' // according to api.coindesk.com 2010-07-17 is the min date from which data is available onwards
}

export default config
