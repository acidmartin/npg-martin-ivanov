/**
 * Application custom events map
 * @module colors
 */

const events = {
  ajax: 'ajax',
  error: 'error'
}

export default events
