/**
 * Application router
 * @module router
 */

import Vue from 'vue'
import Router from 'vue-router'
import Index from '../pages/index'
import About from '../pages/about'
import Bpi from '../pages/bpi'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'index',
    component: Index
  }, {
    path: '/about',
    name: 'about',
    component: About
  }, {
    path: '/bpi/:start/:end',
    name: 'bpi',
    component: Bpi
  }, {
    path: '*',
    redirect: '/'
  }]
})
