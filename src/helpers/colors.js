/**
 * Application colors
 * @module colors
 */

import colorSchemes from './color-schemes'

const colors = {
  ...colorSchemes,
  chartColors: {
    2010: '#f44336',
    2011: '#e91e63',
    2012: '#9c27b0',
    2013: '#673ab7',
    2014: '#3f51b5',
    2015: '#2196f3',
    2016: '#03a9f4',
    2017: '#00bcd4',
    2019: '#009688',
    2020: '#4caf50',
    2021: '#8bc34a',
    2022: '#cddc39',
    2023: '#ffeb3b',
    2024: '#ffc107',
    2025: '#ff9800',
    2026: '#ff5722'
  }
}

export default colors
