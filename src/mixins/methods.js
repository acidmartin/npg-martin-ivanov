/**
 * Methods mixin, available application-wide
 * @module methods
 */

const methods = {
  methods: {
    /**
     * Get color from the current color scheme
     * @method getColorScheme
     * @param {string} color [getColorScheme = 'primary']
     * @return {string}
     */
    getColorScheme (color = 'primary') {
      return this.$root.colors[this.$root.config.colorScheme][color]
    },
    /**
     * Set document.title
     * @method setDocumentTitle
     * @param {string} subtitle [subtitle = '']
     * @param {string} delimiter [delimiter = '|']
     * @return {void}
     */
    setDocumentTitle (subtitle = '', delimiter = ' | ') {
      document.title = `${this.$config.appTitle}${subtitle ? delimiter : ''}${subtitle}`
    },
    /**
     * Get the BPI data by date range
     * @method setDocumentTitle
     * @param {array} start [subtitle = [config.minDate]]
     * @param {array} end [end = [this.$config.today]]
     * @return {string}
     */
    getBpi (start = [this.$config.minDate], end = [this.$config.today]) {
      return `/bpi/${JSON.stringify(start)}/${JSON.stringify(end)}`
    }
  }
}

export default methods
