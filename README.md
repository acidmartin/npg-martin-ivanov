# npg-martin-ivanov

> Visualisations of historic BitCoin data from the CoinDesk API.

## Project Details and Highlights

### BitBucket Repo

> https://bitbucket.org/acidmartin/npg-martin-ivanov/

### Demo Website
  
> https://npg.wemakesites.net

> Make sure you click "advanced > proceed" in case you 
  get the insecure page warning, as I am using a self-signed SSL certificate.

## Task Highlights

> Development took around 20 hours, including documentation, setup, deployment, testing etc.

- Single-page PWA
- Show any historical data depending on the arguments passed to the API, starting with the very first day of data available (2010-07-17)
- The selected time range is persisted in the route, so charts can be shared (also via the "permalink" button above the chart)
- Compare years (multiple year selection)
- Custom date range with date picker
- Fully client-side chart data export to .png, .json and .csv
- Easy configuration, external localization, mixins, custom events, reusable components
- Modern, comprehensive, and self-explanatory UI
- Theme and color scheme switchers, persisted in the local storage
- Responsive and mobile friendly
- Handling of HTTP API errors both in the UI and in the console
- Minimal custom CSS, as Vuetify and Google Material provides everything necessary
- Redirects for non-existent routes to the app root
- The code is fully documented in jsDoc style
- Serving g-zipped and over HTTPS on the demo domain

## Tasks

> Some of the tasks I defined I decided to drop as it is a task 
  assignment, I was time limited. Of course all of those marked as "won't do" are 
  doable. The complete list tasks that I did not do are at the bottom 
  of the "tasks" section of the readme

- [done] Show message on link copied
- [done] Handle HTTP errors, show some message and console output
- [done] Make the repo public when submitting the assignment
- [done] Deploy onto my server at https://npg.wemakesites.net, serve over SSL
- [done] Copy /site-meta-files/robots.txt, /site-meta-filessitemap.xml and /site-meta-files/.htaccess to the dist/ folder at build time
- [done] Add:
  - [done] /robots.txt,
  - [done] /sitemap.xml
  - [done] /.htaccess (cache, mod_rewrite, g-zip)
  - [done] /.favicon and application icons
  - [done] /static/data/manifest.json
  - [done] Add content to the "keywords" and "description" meta-tags
  - [done] Add necessary meta-tags
  - [done] Add the noscript tag
- [done] On the /bpi/:start/:end page show range, translate labels, etc.
- [done] Describe all of the stuff done and the basic functionality here in the README.md file
- [done] Each page in the router should also make a call for page data and meta and show it in the app
- [done] Remove useless console.log() entries
- [done] Add catch() for all promises
- [done] Custom events if need be  
- [done] Add loading overlay for ajax calls
- [done] Add Axios interceptors
- [done] Implement date range picker
- [done] Theme color meta for mobile browsers
- [done] Add some image at the top of the navigation bar
- [done] Add icon to the drop down for the years filter
- [done] Add icons to the export dropdown
- [done] Add options for multiple selections in the years dropdown
- [done] Add custom events file
- [done] Implement light and dark theme switcher
- [done] Implement color scheme selector
- [done] Randomize chart colors depending on the number of properties shown
- [done] Use icon fonts
- [done] Use data from package.json
- [done] Document the code
- [done] Config file, global
- [done] Language file, global
- [done] Redirect non-existent routes to /
- [done] Push to a BitBucket repo
- [done] Add social sharing bar
- [done] Add functionality to export the charts as:
  - [done] .png
  - [done] .json
  - [done] .csv
- [won't do] Add vue-tour for app onboarding
- [won't do] Implement functionality to import chart data as json
- [won't do] Make localizable
- [won't do] Use Vuex for application state (wasn't needed)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
